module.exports = ({customers, address}) => ({
    getAllCustomers: async () => {
        try {
            const dataCustomers = await customers.findAll(
                {
                    include: {model:address}
                }
            );
            return dataCustomers;
        } catch(error){
            throw error;
        }
    },
});
const app = require("./provider/app")
require("dotenv").config();
const mongoose = require("mongoose")

mongoose
    .connect(process.env.MONGO_URI, 
    {
        useNewUrlParser: true,  
        useUnifiedTopology:true,
    }).catch((error) => {
        console.log(error.message)
    });

app.listen(process.env.PORT, () =>{
    console.log(`running on port ${process.env.PORT}`);
});
// init database
const db = require('../../models');
// const products = require("../../schema/product");

// init all services
const customerService = require("../service/customer-service")(db);
// const productService = require("../service/product-service")(products);


// init all controller
const customerController = require("../controller/customer-controller")(customerService);

// const productController = require("../controller/product-controller")(productService);

// init all router
const customerRouter = require("../router/customer-router")(customerController);
// const productRouter = require("../router/product-router")(productController);

module.exports = {
    customerRouter,
    // productRouter
}
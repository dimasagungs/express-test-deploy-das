const express = require("express");
const container = require("./container")


const app = express();

// app.get("/", (req,res) => res.send("hello world"));

app.get("/", (req, res) => res.send("welcome to index page"));
app.get("/info", (req, res) => res.send("this page created by Dimas"));

app.use("/api/customers", container.customerRouter);
// app.use("/api/products", container.productRouter);

module.exports = app;
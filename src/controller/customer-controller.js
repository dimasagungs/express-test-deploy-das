module.exports = (service) => ({
    fetchCustomers: async (req, res) =>
      await service
        .getAllCustomers()
        .then((data) => res.json(data))
        .catch((error) => {
          console.log(error);
          return res.status(500).send("internal server error");
        }),
  });
  
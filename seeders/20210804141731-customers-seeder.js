'use strict';
const faker = require("faker");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    const dataAddress = [];
    const dataCustomers = [];

    for(let i = 0; i < 10; i++){
      
      dataAddress.push(
        {
          name: faker.address.streetName(),
          createdAt : new Date(),
          updatedAt: new Date()
        }
      );
      
      dataCustomers.push(
        {
          name : faker.name.findName(),
          createdAt : new Date(),
          updatedAt: new Date(),
          addressId : i + 1 
        }
      );
    }

    await queryInterface.bulkInsert("addresses", dataAddress)
    await queryInterface.bulkInsert("customers", dataCustomers);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("addresses", null, {});
    await queryInterface.bulkDelete("customers", null, {});

  }
};
